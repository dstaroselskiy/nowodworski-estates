<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Client\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

/*
 * @testdox Proxy controller
 * @coversDefaultClass \App\Http\Controllers\ProxyController
 */
class ProxyControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @var User|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model */
    private null|User $user;

    /**
     * {@inerhitDoc}.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = null;
    }

    /**
     * Return or create and return one user.
     *
     * @return User
     */
    private function getUserForAuth(): User
    {
        if (null == $this->user) {
            $this->user = User::factory()
                ->create();
        }

        return $this->user;
    }

    /**
     * Set external route for testing proxy,
     * and mock response from api link.
     *
     * @param string $externalApiUrl
     */
    private function iniDefaultOptions(
        string $externalApiUrl
    ): void {
        config()->set(
            'services.external.url',
            $externalApiUrl
        );

        Http::fake([
            $externalApiUrl . '/auth/*' => Http::response('Hello World 1', 200, ['Headers']),
            $externalApiUrl . '/api/*' => Http::response('Hello World 2', 200, ['Headers']),
        ]);
    }

    /**
     * Check auth external request information.
     *
     * @param Request $request
     * @param string  $externalApiUrl
     * @param string  $externalApiAction
     *
     * @return bool
     */
    private function checkExternalAuthRequest(
        Request $request,
        string $externalApiUrl,
        string $externalApiAction
    ): bool {
        $returnValue = ('POST' === $request->method())
            && ($request->url() === $externalApiUrl . '/auth/' . $externalApiAction);

        if ($returnValue) {
            $user = $this->getUserForAuth();
            $requestData = $request->data();
            $this->assertEquals($requestData['name'], $user->getAttribute('name'), 'User Name for api is not correct');
            $this->assertEquals($requestData['email'], $user->getAttribute('email'), 'User Email for api is not correct');
            $this->assertEquals($requestData['id'], $user->getAttribute('id'), 'User Id for api is not correct');
        }

        return $returnValue;
    }

    /**
     * Testing sending request by proxy controller to external api
     * without auth user.
     *
     * @return void
     */
    public function testCallWithOutAuthUser(): void
    {
        Http::fake(function ($request) {
            return Http::response('Hello World', 200);
        });

        $response = $this->get('proxy/getOrders1');

        $this->assertTrue(200 != $response->getStatusCode());
    }

    /**
     * Testing sending empty request by proxy controller to external api.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["get", "https://api.nowodworski-estates.test.get-order", "get-order"]
     *                  ["post", "https://api.nowodworski-estates.test.get-order", "get-order"]
     *                  ["put", "https://api.nowodworski-estates.test.get-order", "get-order"]
     *                  ["patch", "https://api.nowodworski-estates.test.get-order", "get-order"]
     *                  ["delete", "https://api.nowodworski-estates.test.get-order", "get-order"]
     *
     * @return void
     */
    public function testSendingEmptyDataByProxy(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_01' => 'proxy_cookies_01<=>' . $methodToCall,
            ])
            ->call(mb_strtoupper($methodToCall), 'proxy/' . $externalApiAction);

        $this->assertTrue(
            200 == $response->status(),
            'Can not send empty data to proxy by method: ' . $methodToCall
        );

        if ('head' == $methodToCall) {
            $this->assertEmpty(
                $response->getContent(),
                'Not correct response content for method: ' . $methodToCall
            );
        } else {
            $this->assertEquals(
                $response->getContent(),
                'Hello World 2',
                'Not correct response content for method: ' . $methodToCall
            );
        }

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall
            ): bool {
                $requestData = $request->data();

                return
                    (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ! $request->hasFile('file')
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && (0 == count($requestData));
            },
        ]);
    }

    /**
     * Testing sending empty request by proxy controller to external api
     * use request method `head`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["head", "https://api.nowodworski-estates.test.get-order", "get-order"]
     *
     * @return void
     */
    public function testSendingEmptyDataByProxyMethodHead(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_01' => 'proxy_cookies_01<=>' . $methodToCall,
            ])
            ->call(mb_strtoupper($methodToCall), 'proxy/' . $externalApiAction);

        $this->assertTrue(
            200 == $response->status(),
            'Can not send empty data to proxy by method: ' . $methodToCall
        );

        $this->assertEmpty(
            $response->getContent(),
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall
            ): bool {
                $requestData = $request->data();

                return
                    (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ! $request->hasFile('file')
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && (0 == count($requestData));
            },
        ]);
    }

    /**
     * Testing sending empty request by proxy controller to external api
     * use request method `options`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["options", "https://api.nowodworski-estates.test.get-order", "get-order"]
     *
     * @return void
     */
    public function testSendingEmptyDataByProxyResponse404(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_01' => 'proxy_cookies_01<=>' . $methodToCall,
            ])
            ->call(mb_strtoupper($methodToCall), 'proxy/' . $externalApiAction);

        $this->assertTrue(
            404 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Method `options` is not support.',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertNothingSent();
    }

    /**
     * Testing sending form values
     * by proxy controller to external api.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["get", "https://api.nowodworski-estates.test.test-form", "sendForm"]
     *                  ["delete", "https://api.nowodworski-estates.test.test-form", "sendForm"]
     *                  ["post", "https://api.nowodworski-estates.test.test-form", "sendForm"]
     *                  ["put", "https://api.nowodworski-estates.test.test-form", "sendForm"]
     *                  ["patch", "https://api.nowodworski-estates.test.test-form", "sendForm"]
     *
     * @return void
     */
    public function testSendingFormValuesByProxy(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_03' => 'proxy_cookies_02 <=>' . $methodToCall,
            ])
            ->call(
                mb_strtoupper($methodToCall),
                'proxy/' . $externalApiAction,
                [
                    'field_01' => '10',
                    'field_02' => '20',
                    'field_03' => '30',
                ], [
                    'proxy_cookies_03' => 'proxy_cookies_03 <=>' . $methodToCall,
                ]
            );
        $this->assertTrue(
            200 == $response->status(),
            'Can not send file to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Hello World 2',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
            ): bool {
                $requestData = $request->data();

                return
                    (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ! $request->hasFile('file')
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && (0 == count($requestData));
            },
        ]);
    }

    /**
     * Testing sending form values
     * by proxy controller to external api
     * use request method `get` and `delete.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @return void
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["get", "https://api.nowodworski-estates.test.test-form", "sendForm"]
     *                  ["delete", "https://api.nowodworski-estates.test.test-form", "sendForm"]
     */
    public function testSendingFormValuesByProxyRequestWithOutFiles(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_03' => 'proxy_cookies_02 <=>' . $methodToCall,
            ])
            ->call(
                mb_strtoupper($methodToCall),
                'proxy/' . $externalApiAction,
                [
                    'field_01' => '10',
                    'field_02' => '20',
                    'field_03' => '30',
                ], [
                    'proxy_cookies_03' => 'proxy_cookies_03 <=>' . $methodToCall,
                ]
            );
        $this->assertTrue(
            200 == $response->status(),
            'Can not send file to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Hello World 2',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
            ): bool {
                $requestData = $request->data();

                return
                    (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && ! $request->hasFile('file')
                    && (0 == count($requestData));
            },
        ]);
    }

    /**
     * Testing sending form values
     * by proxy controller to external api
     * use request method `head`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["head", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     *
     * @return void
     */
    public function testSendingFormValuesByProxyMethodHead(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_03' => 'proxy_cookies_02 <=>' . $methodToCall,
            ])
            ->call(
                mb_strtoupper($methodToCall),
                'proxy/' . $externalApiAction,
                [
                    'field_01' => '10',
                    'field_02' => '20',
                    'field_03' => '30',
                ], [
                    'proxy_cookies_03' => 'proxy_cookies_03 <=>' . $methodToCall,
                ]
            );
        $this->assertTrue(
            200 == $response->status(),
            'Can not send file to proxy by method: ' . $methodToCall
        );

        $this->assertEmpty(
            $response->getContent(),
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
            ): bool {
                $requestData = $request->data();

                return
                    (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && ! $request->hasFile('file')
                    && (0 == count($requestData));
            },
        ]);
    }

    /**
     * Testing sending form values
     * by proxy controller to external api
     * use request method `options`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["options", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     *
     * @return void
     */
    public function testSendingFormValuesByProxyResponse404(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_03' => 'proxy_cookies_02 <=>' . $methodToCall,
            ])
            ->call(
                mb_strtoupper($methodToCall),
                'proxy/' . $externalApiAction,
                [
                    'field_01' => '10',
                    'field_02' => '20',
                    'field_03' => '30',
                ], [
                    'proxy_cookies_03' => 'proxy_cookies_03 <=>' . $methodToCall,
                ],
            );

        $this->assertTrue(
            404 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Method `options` is not support.',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertNothingSent();
    }

    /**
     * Testing sending files with form values
     * by proxy controller to external api.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["post", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     *                  ["put", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     *                  ["patch", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     *
     * @return void
     */
    public function testSendingFileByProxy(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        $sendFile = UploadedFile::fake()->image('avatar.jpg');
        $sendFile2 = UploadedFile::fake()->image('avatar3.jpg');

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_03' => 'proxy_cookies_02 <=>' . $methodToCall,
            ])
            ->call(
                mb_strtoupper($methodToCall),
                'proxy/' . $externalApiAction,
                [
                    'field_01' => '10',
                    'field_02' => '20',
                    'field_03' => '30',
                ], [
                    'proxy_cookies_03' => 'proxy_cookies_03 <=>' . $methodToCall,
                ], [
                    'file' => $sendFile,
                    'file2' => $sendFile2,
                ]
            );

        $this->assertTrue(
            200 == $response->status(),
            'Can not send file to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Hello World 2',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
                $sendFile,
            ): bool {
                $requestData = $request->data();

                $returnValue = (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && $request->hasFile('file')
                    && ! $request->hasFile('file2')
                    && (1 == count($requestData))
                    && (3 == count($requestData[0]));

                if ($returnValue) {
                    $this->assertEqualsCanonicalizing([[
                        'name' => 'file',
                        'contents' => $sendFile->getContent(),
                        'filename' => 'avatar.jpg',
                    ]], $request->data());
                }

                return $returnValue;
            },
        ]);
    }

    /**
     * Testing sending files with form values
     * by proxy controller to external api
     * use request method `get` and `delete.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @return void
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["get", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     *                  ["delete", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     */
    public function testSendingFileByProxyRequestWithOutFiles(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        $sendFile = UploadedFile::fake()->image('avatar.jpg');
        $sendFile2 = UploadedFile::fake()->image('avatar3.jpg');

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_03' => 'proxy_cookies_02 <=>' . $methodToCall,
            ])
            ->call(
                mb_strtoupper($methodToCall),
                'proxy/' . $externalApiAction,
                [
                    'field_01' => '10',
                    'field_02' => '20',
                    'field_03' => '30',
                ], [
                    'proxy_cookies_03' => 'proxy_cookies_03 <=>' . $methodToCall,
                ], [
                    'file' => $sendFile,
                    'file2' => $sendFile2,
                ]
            );
        $this->assertTrue(
            200 == $response->status(),
            'Can not send file to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Hello World 2',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall
                ,
            ): bool {
                $requestData = $request->data();

                return
                    (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && ! $request->hasFile('file')
                    && ! $request->hasFile('file2')
                    && (0 == count($requestData));
            },
        ]);
    }

    /**
     * Testing sending files with form values
     * by proxy controller to external api
     * use request method `head`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["head", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     *
     * @return void
     */
    public function testSendingFileByProxyMethodHead(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        $sendFile = UploadedFile::fake()->image('avatar.jpg');
        $sendFile2 = UploadedFile::fake()->image('avatar3.jpg');

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_03' => 'proxy_cookies_02 <=>' . $methodToCall,
            ])
            ->call(
                mb_strtoupper($methodToCall),
                'proxy/' . $externalApiAction,
                [
                    'field_01' => '10',
                    'field_02' => '20',
                    'field_03' => '30',
                ], [
                    'proxy_cookies_03' => 'proxy_cookies_03 <=>' . $methodToCall,
                ], [
                    'file' => $sendFile,
                    'file2' => $sendFile2,
                ]
            );
        $this->assertTrue(
            200 == $response->status(),
            'Can not send file to proxy by method: ' . $methodToCall
        );

        $this->assertEmpty(
            $response->getContent(),
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall
                ,
            ): bool {
                $requestData = $request->data();

                return
                    (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && ! $request->hasFile('file')
                    && ! $request->hasFile('file2')
                    && (0 == count($requestData));
            },
        ]);
    }

    /**
     * Testing sending files with form values
     * by proxy controller to external api
     * use request method `options`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["options", "https://api.nowodworski-estates.test.test-file", "sendFile"]
     *
     * @return void
     */
    public function testSendingFileByProxyResponse404(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        $sendFile = UploadedFile::fake()->image('avatar.jpg');
        $sendFile2 = UploadedFile::fake()->image('avatar3.jpg');

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_03' => 'proxy_cookies_02 <=>' . $methodToCall,
            ])
            ->call(
                mb_strtoupper($methodToCall),
                'proxy/' . $externalApiAction,
                [
                    'field_01' => '10',
                    'field_02' => '20',
                    'field_03' => '30',
                ], [
                    'proxy_cookies_03' => 'proxy_cookies_03 <=>' . $methodToCall,
                ], [
                    'file' => $sendFile,
                    'file2' => $sendFile2,
                ]
            );

        $this->assertTrue(
            404 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Method `options` is not support.',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertNothingSent();
    }

    /**
     * Testing sending json content
     * by proxy controller to external api.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     * @param array  $sendData
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["post", "https://api.nowodworski-estates.test.test-content", "sendJsonContent", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *                  ["put", "https://api.nowodworski-estates.test.test-content", "sendJsonContent", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *                  ["patch", "https://api.nowodworski-estates.test.test-content", "sendJsonContent", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *                  ["delete", "https://api.nowodworski-estates.test.test-content", "sendJsonContent", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *
     * @return void
     */
    public function testSendingJsonContentByProxy(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction,
        array $sendData
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_05' => 'proxy_cookies_05 <=>' . $methodToCall,
            ])
            ->json(
                $methodToCall,
                'proxy/' . $externalApiAction,
                $sendData,
                [
                    'proxy_header_06' => 'proxy_header_06 <=>' . $methodToCall,
                ]
            );

        $this->assertTrue(
            200 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Hello World 2',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
                $sendData
            ): bool {
                $returnValue = (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && ! $request->hasFile('file')
                    && (count($request->data()) == count($sendData));

                if ($returnValue) {
                    $this->assertEqualsCanonicalizing($sendData, $request->data());
                }

                return $returnValue;
            },
        ]);
    }

    /**
     * Testing sending json content
     * by proxy controller to external api.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     * @param array  $sendData
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["get", "https://api.nowodworski-estates.test.test-content", "sendJsonContent", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *
     * @return void
     */
    public function testSendingJsonContentByProxyUrlChanged(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction,
        array $sendData
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_05' => 'proxy_cookies_05 <=>' . $methodToCall,
            ])
            ->json(
                $methodToCall,
                'proxy/' . $externalApiAction,
                $sendData,
                [
                    'proxy_header_06' => 'proxy_header_06 <=>' . $methodToCall,
                ]
            );

        $this->assertTrue(
            200 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Hello World 2',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
                $sendData
            ): bool {
                $returnValue = (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction . '?' . http_build_query($sendData))
                    && ! $request->hasFile('file')
                    && (count($request->data()) == count($sendData));

                if ($returnValue) {
                    $this->assertEqualsCanonicalizing($sendData, $request->data());
                }

                return $returnValue;
            },
        ]);
    }

    /**
     * Testing sending json content
     * by proxy controller to external api.
     * use request method `head`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     * @param array  $sendData
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["head", "https://api.nowodworski-estates.test.test-content", "sendJsonContent", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *
     * @return void
     */
    public function testSendingJsonContentByProxyMethodHead(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction,
        array $sendData
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_05' => 'proxy_cookies_05 <=>' . $methodToCall,
            ])
            ->json(
                $methodToCall,
                'proxy/' . $externalApiAction,
                $sendData,
                [
                    'proxy_header_06' => 'proxy_header_06 <=>' . $methodToCall,
                ]
            );

        $this->assertTrue(
            200 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEmpty(
            $response->getContent(),
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
                $sendData
            ): bool {
                $requestData = $request->data();

                $returnValue = (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction . '?' . http_build_query($sendData))
                    && ! $request->hasFile('file')
                    && (3 == count($requestData));

                if ($returnValue) {
                    $this->assertEqualsCanonicalizing($sendData, $request->data());
                }

                return $returnValue;
            },
        ]);
    }

    /**
     * Testing sending json content
     * by proxy controller to external api
     * use request method `options`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     * @param array  $sendData
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["options", "https://api.nowodworski-estates.test.test-content", "sendJsonContent", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *
     * @return void
     */
    public function testSendingJsonContentByProxyResponse404(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction,
        array $sendData
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_05' => 'proxy_cookies_05 <=>' . $methodToCall,
            ])
            ->json(
                $methodToCall,
                'proxy/' . $externalApiAction,
                $sendData,
                [
                    'proxy_header_06' => 'proxy_header_06 <=>' . $methodToCall,
                ]
            );

        $this->assertTrue(
            404 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Method `options` is not support.',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertNothingSent();
    }

    /**
     * Testing sending json content with files
     * by proxy controller to external api.
     * use request method `get`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     * @param array  $sendData
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["get", "https://api.nowodworski-estates.test.test-content-with-files", "sendJsonContentWithFiles", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *
     * @return void
     */
    public function testSendingJsonContentWithFilesByProxyUrlChanged(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction,
        array $sendData
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        $sendFile = UploadedFile::fake()->image('avatar.jpg');
        $sendFile2 = UploadedFile::fake()->image('avatar3.jpg');

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_05' => 'proxy_cookies_05 <=>' . $methodToCall,
            ])
            ->json(
                $methodToCall,
                'proxy/' . $externalApiAction,
                array_merge($sendData, [
                    'file' => $sendFile,
                    'file2' => $sendFile2,
                ]), [
                    'proxy_header_06' => 'proxy_header_06 <=>' . $methodToCall,
                ]
            );

        $this->assertTrue(
            200 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Hello World 2',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
                $sendData
            ): bool {
                $returnValue = (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction . '?' . http_build_query($sendData))
                    && ! $request->hasFile('file')
                    && ! $request->hasFile('file1')
                    && (3 == count($request->data()));

                if ($returnValue) {
                    $this->assertEqualsCanonicalizing($sendData, $request->data());
                }

                return $returnValue;
            },
        ]);
    }

    /**
     * Testing sending json content with files
     * by proxy controller to external api.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     * @param array  $sendData
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith        ["post", "https://api.nowodworski-estates.test.test-content-with-files", "sendJsonContentWithFiles", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *                  ["put", "https://api.nowodworski-estates.test.test-content-with-files", "sendJsonContentWithFiles", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *                  ["patch", "https://api.nowodworski-estates.test.test-content-with-files", "sendJsonContentWithFiles", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *                  ["delete", "https://api.nowodworski-estates.test.test-content-with-files", "sendJsonContentWithFiles", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *
     * @return void
     */
    public function testSendingJsonContentWithFilesByProxy(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction,
        array $sendData
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        $sendFile = UploadedFile::fake()->image('avatar.jpg');
        $sendFile2 = UploadedFile::fake()->image('avatar3.jpg');

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_05' => 'proxy_cookies_05 <=>' . $methodToCall,
            ])
            ->json(
                $methodToCall,
                'proxy/' . $externalApiAction,
                array_merge($sendData, [
                    'file' => $sendFile,
                    'file2' => $sendFile2,
                ]), [
                    'proxy_header_06' => 'proxy_header_06 <=>' . $methodToCall,
                ]
            );

        $this->assertTrue(
            200 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Hello World 2',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
                $sendData,
                $sendFile
            ): bool {
                $returnValue = (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction)
                    && $request->hasFile('file')
                    && ! $request->hasFile('file1')
                    && (4 == count($request->data()));

                if ($returnValue) {
                    $searchRequestData = [];
                    foreach ($sendData as $key => $row) {
                        $searchRequestData[] = [
                            'name' => $key,
                            'contents' => $row,
                        ];
                    }
                    $searchRequestData[] = [
                        'name' => 'file',
                        'contents' => $sendFile->getContent(),
                        'filename' => 'avatar.jpg',
                    ];

                    $this->assertEqualsCanonicalizing($searchRequestData, $request->data());
                }

                return $returnValue;
            },
        ]);
    }

    /**
     * Testing sending json content wit files
     * by proxy controller to external api
     * use request method `options`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     * @param array  $sendData
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["options", "https://api.nowodworski-estates.test.test-content-with-files", "sendJsonContentWithFiles", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *
     * @return void
     */
    public function testSendingJsonContentWithFilesByProxyResponse404(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction,
        array $sendData
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        $sendFile = UploadedFile::fake()->image('avatar.jpg');
        $sendFile2 = UploadedFile::fake()->image('avatar3.jpg');

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_05' => 'proxy_cookies_05 <=>' . $methodToCall,
            ])
            ->json(
                $methodToCall,
                'proxy/' . $externalApiAction,
                array_merge($sendData, [
                    'file' => $sendFile,
                    'file2' => $sendFile2,
                ]), [
                    'proxy_header_06' => 'proxy_header_06 <=>' . $methodToCall,
                ]
            );

        $this->assertTrue(
            404 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEquals(
            $response->getContent(),
            'Method `options` is not support.',
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertNothingSent();
    }

    /**
     * Testing sending json content with files
     * by proxy controller to external api.
     * use request method `head`.
     *
     * @param string $methodToCall
     * @param string $externalApiUrl
     * @param string $externalApiAction
     * @param array  $sendData
     *
     * @covers ::proxy
     * @group proxyController
     * @testWith ["head", "https://api.nowodworski-estates.test.test-content-with-files", "sendJsonContentWithFiles", {"field_01":"10", "field_02":"20", "field_03":"30"}]
     *
     * @return void
     */
    public function testSendingJsonContentWithFilesByProxyMethodHead(
        string $methodToCall,
        string $externalApiUrl,
        string $externalApiAction,
        array $sendData
    ): void {
        $this->iniDefaultOptions($externalApiUrl);

        $sendFile = UploadedFile::fake()->image('avatar.jpg');
        $sendFile2 = UploadedFile::fake()->image('avatar3.jpg');

        /** @var TestResponse $response */
        $response = $this
            ->actingAs($this->getUserForAuth())
            ->withCookies([
                'proxy_cookies_05' => 'proxy_cookies_05 <=>' . $methodToCall,
            ])
            ->json(
                $methodToCall,
                'proxy/' . $externalApiAction,
                array_merge($sendData, [
                    'file' => $sendFile,
                    'file2' => $sendFile2,
                ]), [
                    'proxy_header_06' => 'proxy_header_06 <=>' . $methodToCall,
                ]
            );

        $this->assertTrue(
            200 == $response->status(),
            'Can not send data to proxy by method: ' . $methodToCall
        );

        $this->assertEmpty(
            $response->getContent(),
            'Not correct response content for method: ' . $methodToCall
        );

        Http::assertSentInOrder([
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction
            ): bool {
                return $this->checkExternalAuthRequest(
                    $request,
                    $externalApiUrl,
                    $externalApiAction
                );
            },
            function (
                Request $request
            ) use (
                $externalApiUrl,
                $externalApiAction,
                $methodToCall,
                $sendData
            ): bool {
                $requestData = $request->data();

                $returnValue = (mb_strtoupper($request->method()) === mb_strtoupper($methodToCall))
                    && ($request->url() === $externalApiUrl . '/api/' . $externalApiAction . '?' . http_build_query($sendData))
                    && ! $request->hasFile('file')
                    && ! $request->hasFile('file1')
                    && (3 == count($requestData));

                if ($returnValue) {
                    $this->assertEqualsCanonicalizing($sendData, $request->data());
                }

                return $returnValue;
            },
        ]);
    }
}
